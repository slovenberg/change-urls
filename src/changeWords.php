<?php

namespace slovenberg\changewords;

use slovenberg\changewords\exceptions\file\FileReadException;
use slovenberg\changewords\exceptions\file\PermissionChangesException;
use slovenberg\changewords\exceptions\file\FileWriteException;
use slovenberg\changewords\exceptions\HandleDirectoryException;
use slovenberg\changewords\exceptions\ProgramMainException;
use slovenberg\changewords\exceptions\TemplateFileException;
use slovenberg\changewords\exceptions\console\ConsoleException;
use slovenberg\changewords\exceptions\console\ConsoleArgumentsException;


class changeWords {

	protected static $tmp_extention = 'tmp';
	protected static $default_extention = 'php';
	protected static $file_mod = 0664;
	protected static $file_user = '';
	protected static $file_group = 'www-data';
	protected static $need_os_config = true;


	protected static $needle = '';
	protected static $replacement = '';


	protected function setConfing() {
		$os_info = php_uname();
		$result = strpos($os_info, 'Linux');
		if($result === false) {
			static::$need_os_config = false;
			return 1;
		}
		static::$file_user = get_current_user();
		return 1;
	}

	protected static function handleConsoleArgumets(array $args_array)
	{
		array_shift($args_array);
		try 
		{
			if(count($args_array) !== 2)
			{
				throw new ConsoleArgumentsException('Неверное число аргументов');
			}
			return ['needle' => $args_array[0], 'replacement' => $args_array[1]];
		} catch(ConsoleArgumentsException $exc)
		{
			throw new ConsoleException('', 0, $exc);
		}
	}

	public static function start() {
		global $argv;

		try 
		{
			$not_depend_argv = $argv;

			$prepared_args = static::handleConsoleArgumets($not_depend_argv);
			extract($prepared_args, EXTR_PREFIX_SAME, 'wddx');
			
			static::$needle = $needle;
			static::$replacement = $replacement;

			# default __DIR__
			$root = '/var/www/change-words/examples/test-dir';
			
			static::setConfing();
			static::getChildrenFiles($root);
		
		} catch(HandleDirectoryException|ConsoleException $exc)
		{
			throw new ProgramMainException('', 0, $exc);
		}
	}

	protected function setupLinuxFile($path) 
	{
		try {
			chmod($path, static::$file_mod);
			chgrp($path, static::$file_group);
			chown($path, static::$file_user);
		} catch(\Exception $exc) {
			throw new PermissionChangesException($path, 0, $exc);
		}
	}

	protected function addNewFile($path)
	{
		try 
		{
			if(!$template_fd = fopen($path, 'w+'))
				throw new FileWriteException($path);
			if(static::$need_os_config)
				static::setupLinuxFile($path);
			fclose($template_fd);
		} catch(FileWriteException|PermissionChangesException $exc)
		{
			throw new TemplateFileException($path, 0, $exc);
		}

	}

	protected static function prepareFile($path) 
	{
		// Создаём путь к временному файлу
		$template_path = rtrim($path, static::$default_extention) . static::$tmp_extention;
		// Основной файл
		$file_path = $path;

		// Путь к ошибочному файлу
		$error_file = '';
		try
		{
			// Сначала обработываем его
			$error_file = $file_path;

			// Если основной файл читаемый - создаём дескриптор
			if(is_readable($file_path))
				$fd_cur = fopen($file_path, 'r+');
			else
			{
				throw new FileReadException($file_path);
			}

			// Меняем отслеживаемый файл
			$error_file = $template_path;

			// Регистрируем временный файл
			static::addNewFile($template_path);

			// Если временный файл записываемый - создаём дескриптор
			if(is_writable($template_path))
				$fd_new = fopen($template_path, 'a+');
			else
				throw new FileWriteException($template_path);
		} catch(FileReadException|TemplateFileException $exc)
		{
			throw new HandleFileException($error_file, 0, $exc);
		}

		// Проходим по строкам старого файла
		while(!feof($fd_cur))
		{
			// Получаем строку
			$string = fgets($fd_cur);
			// Заменяем в ней одно выражение на другое
			$rewrited_string = str_replace(static::$needle, static::$replacement, $string);
			// Вставляем новую строку во временный файл
			fputs($fd_new, $rewrited_string);
		}
		fclose($fd_new);
		fclose($fd_cur);

		unlink($file_path);
		rename($template_path, $file_path);
	}

	protected static function getChildrenFiles($cur_dir) {
		// Сканируем текущую директорию
		$file_descs = scandir($cur_dir);
		// Обрезаем возвратные пути
		$file_descs = array_diff($file_descs, ['.', '..']);
		// Проходимся по всем файлам
		foreach ($file_descs as $file_desc) {
			// Получаем новый путь
			$new_path = $cur_dir . DIRECTORY_SEPARATOR . $file_desc;
			if(is_dir($new_path)) {
				try
				{
				static::getChildrenFiles($new_path);
				} catch(HandleDirectoryException $exc)
				{
					throw new HandleDirectoryException($new_path, 0, $exc);
				}
			} else {
				if(pathinfo($new_path)['extension'] == static::$default_extention)
					try
					{
						static::prepareFile($new_path);
					} catch(HandleFileException $exc) 
					{
						throw new HandleDirectoryException($new_path, 0, $exc);
					}
			}
		}
	}
}