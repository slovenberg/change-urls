<?php

namespace slovenberg\changewords\exceptions\file;


class FileReadException extends \Exception 
{
	public function __construct($message='', $code=0, $previous=null)
	{
		parent::__construct($message, $code, $previous);
		$this->message = "Произошла ошибка при чтении файла: " . (string)$message;
	}
}