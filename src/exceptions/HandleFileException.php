<?php


namespace slovenberg\changewords\exceptions;



class HandleFileException extends \Exception
{
	public function __construct($message='', $code=0, $previous=null)
	{
		parent::__construct($message, $code, $previous);
		$this->message = "Произошла ошибка при обработке файла: " . $message;
	}
}