<?php

namespace slovenberg\changewords\exceptions\console;


class ConsoleException extends \Exception 
{
	public function __construct($message='', $code=0, $previous=null)
	{
		parent::__construct($message, $code, $previous);
		$this->message = "Произошла консольная ошибка";
	}
}