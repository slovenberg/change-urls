<?php


namespace slovenberg\changewords\exceptions;



class TemplateFileException extends \Exception
{
	public function __construct($message='', $code=0, $previous=null)
	{
		parent::__construct($message, $code, $previous);
		$this->message = "Произошла ошибка при создании временного файла: " . $message;
	}
}